
// BAI 1

// console.log(so2)
// console.log(so3)

// console.log(document.getElementById());
document.getElementById('btnKqBai1').addEventListener("click", function () {
    var so1 = document.querySelector(".so1").value * 1;
    var so2 = document.querySelector(".so2").value * 1;
    var so3 = document.querySelector(".so3").value * 1;
    var t1;
    var t2;
    var t3;
    // console.log(so1);
    // console.log(so2);
    // console.log(so3);
    if (so1 > so2 && so2 > so3) {
        t3 = so1;
        t2 = so2;
        t1 = so3;
    }
    else if (so1 == so2 && so1 < so3) {
        t3 = so3;
        t2 = so1;
        t1 = so2;

    }
    else if (so1 == so2 && so1 > so3) {
        t3 = so2;
        t2 = so1;
        t1 = so3;
    }

    else if (so1 == so3 && so1 > so2) {
        t3 = so3;
        t2 = so1;
        t1 = so2;
    }
    else if (so1 == so3 && so1 < so2) {
        t3 = so2;
        t2 = so1;
        t1 = so3;
    }
    else if (so3 == so2 && so3 > so1) {
        t3 = so2;
        t2 = so3;
        t1 = so1;
    }
    else if (so3 == so2 && so3 < so1) {
        t3 = so1;
        t2 = so3;
        t1 = so2;
    }
    else if (so1 < so2 && so2 < so3) {
        t3 = so3;
        t2 = so2;
        t1 = so1;
    }

    else if (so1 < so3 && so1 > so2) {
        t3 = so3;
        t2 = so1;
        t1 = so2;
    }
    else if (so3 < so1 && so3 > so2) {
        t3 = so1;
        t2 = so3;
        t1 = so2;
    }
    else if (so2 > so1 && so1 > so3) {
        t3 = so2;
        t2 = so1;
        t1 = so3;
    }
    document.querySelector(".kqBai1").innerHTML = `${t1} ${t2} ${t3}`
});



// BAI 2
document.getElementById('btnKqBai2').addEventListener("click", function () {
    var member = document.getElementById('memberFamily').value * 1;
    // console.log(member);
    // var content = "";
    switch (member) {
        case 1: {
            content = "Chào Bố";
            break;
        }
        case 2: {
            content = "Chào Mẹ";
            break;
        }
        case 3: {
            content = "Chào anh trai";
            break;
        }
        case 4: {
            content = "Chào em gái";
            break;
        }
    }
    document.querySelector(".kqBai2").innerHTML = `${content}`;
})
// BAI 3
document.getElementById('btnKqBai3').addEventListener("click", function () {
    var so1 = document.querySelector('#BT-3 .so1').value * 1;
    var so2 = document.querySelector('#BT-3 .so2').value * 1;
    var so3 = document.querySelector('#BT-3 .so3').value * 1;
    var soLe;
    var soChan;
    if (so1 % 2 == 0 && so2 % 2 == 0 && so3 % 2 == 0) {
        soLe = 0;
        soChan = 3;
    } else if (so1 % 2 != 0 && so2 % 2 != 0 && so3 % 2 != 0) {
        soLe = 3;
        soChan = 0;
    } else if (so1 % 2 != 0 && so2 % 2 != 0 && so3 % 2 == 0) {
        soLe = 2;
        soChan = 1;
    }
    else if (so1 % 2 != 0 && so2 % 2 == 0 && so3 % 2 == 0) {
        soLe = 1;
        soChan = 2;
    }
    else if (so1 % 2 == 0 && so2 % 2 != 0 && so3 % 2 == 0) {
        soLe = 2;
        soChan = 1;
    }
    else if (so1 % 2 == 0 && so2 % 2 == 0 && so3 % 2 != 0) {
        soLe = 1;
        soChan = 2;
    }
    else if (so1 % 2 != 0 && so2 % 2 == 0 && so3 % 2 != 0) {
        soLe = 2;
        soChan = 1;
    }
    document.querySelector(".kqBai3").innerHTML = `So chan: ${soChan}; So Le: ${soLe}`;


})
// BAI 4
document.getElementById('btnKqBai4').addEventListener("click", function () {
    var a = document.querySelector('.canh1').value * 1;
    var b = document.querySelector('.canh2').value * 1;
    var c = document.querySelector('.canh3').value * 1;
    var tenHinh = "";
    if (a == 3 && a == b && b == c) {
        tenHinh = "Tam giác đều";
    }
    else if (a == 2 && a == b && c == 1) {
        tenHinh = "Tam giác cân";
    }
    else if (a == 3 && b == 4 && c == 5) {
        tenHinh = "Tam giác vuông";
    }
    else {
        tenHinh = "khong ton tai";
    }
    document.querySelector(".kqBai4").innerHTML = `${tenHinh}`;

})
const BT_1 = document.querySelector('.BT_1');
const BT_1_Active = document.getElementById('BT-1');
const BT_2 = document.querySelector('.BT_2');
const BT_2_Active = document.getElementById('BT-2');
const BT_3 = document.querySelector('.BT_3');
const BT_3_Active = document.getElementById('BT-3');
const BT_4 = document.querySelector('.BT_4');
const BT_4_Active = document.getElementById('BT-4');


BT_1.addEventListener('click', () => {
    BT_1_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
})

BT_2.addEventListener('click', () => {
    BT_2_Active.classList.toggle('active')
    BT_1_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
})

BT_3.addEventListener('click', () => {
    BT_3_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_1_Active.classList.remove('active')
    BT_4_Active.classList.remove('active')
})

BT_4.addEventListener('click', () => {
    BT_4_Active.classList.toggle('active')
    BT_2_Active.classList.remove('active')
    BT_3_Active.classList.remove('active')
    BT_1_Active.classList.remove('active')
})
